package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldReturnCorrectValue()
    {
        assertEquals("Value is different what you expected", 5, App.echo(5));

    }
    @Test
    public void textOneMoreMethod ()
    {
        assertEquals("Value is different what you expected", 6, App.oneMore(5));
    }
}
